package com.example.roomdatabase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.provider.Settings;

import com.example.roomdatabase.activity.AlarmUi;

import java.io.Serializable;

public class AlertReceiver extends BroadcastReceiver implements UpdateListener{

    MediaPlayer mediaPlayer;
    UpdateListener listener;

    @Override
    public void onReceive(Context context, Intent intent) {
        mediaPlayer = MediaPlayer.create(context, Settings.System.DEFAULT_RINGTONE_URI);
        mediaPlayer.start();
        listener = this;

        CountDownTimer countDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                stopMediaPlayer();
            }
        };
        countDownTimer.start();
        String mediName = intent.getStringExtra("medicineName");
        Intent intent1 = new Intent(context, AlarmUi.class);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent1.putExtra("medicineName", mediName);
        // intent1.putExtra("listener", (Serializable) listener);
        context.startActivity(intent1);
    }

    public void stopMediaPlayer(){
        mediaPlayer.stop();
    }

    @Override
    public void update() {
        stopMediaPlayer();
    }
}
