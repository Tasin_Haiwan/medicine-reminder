package com.example.roomdatabase;

public class MedicineInfo {
    private String medicineName;
    private int reqCode;

    public MedicineInfo(String medicineName, Object reqCode) {
        this.medicineName = medicineName;
        this.reqCode = (int) reqCode;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public int getReqCode() {
        return reqCode;
    }

    public void setReqCode(int reqCode) {
        this.reqCode = reqCode;
    }
}
