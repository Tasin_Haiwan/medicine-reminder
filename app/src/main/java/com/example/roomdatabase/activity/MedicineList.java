package com.example.roomdatabase.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Update;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.roomdatabase.CancelListener;
import com.example.roomdatabase.MedicineAdapter;
import com.example.roomdatabase.MedicineInfo;
import com.example.roomdatabase.R;
import com.example.roomdatabase.UpdateListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MedicineList extends AppCompatActivity implements UpdateListener {

    RecyclerView recyclerView;
    SharedPreferences sharedPreferences;
    MedicineAdapter medicineAdapter;
    private List<MedicineInfo> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_list);

        sharedPreferences = this.getSharedPreferences(this.getString(R.string.spMedicines), Context.MODE_PRIVATE);
        list = new ArrayList<>();

        recyclerView = findViewById(R.id.recycler_view);
        medicineAdapter = new MedicineAdapter(this, list, sharedPreferences);
        recyclerView.setAdapter(medicineAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        medicineAdapter.setUpdateListener(this);

        fetchList();
    }

    @Override
    public void update() {
        fetchList();
    }

    private void fetchList(){
        //It fetches the medicine list and update the list again.

        Map<String, ?> mp = sharedPreferences.getAll();
        List<MedicineInfo> list = new ArrayList<>();
        for(Map.Entry<String, ?> entry : mp.entrySet()){
            // list.add((String) entry.getValue());
            MedicineInfo medicineInfo = new MedicineInfo(entry.getKey(), entry.getValue());
            list.add(medicineInfo);
        }
        medicineAdapter.setList(list);
    }
}