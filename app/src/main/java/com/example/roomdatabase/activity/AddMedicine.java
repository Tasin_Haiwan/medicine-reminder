package com.example.roomdatabase.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.roomdatabase.AlertReceiver;
import com.example.roomdatabase.CancelListener;
import com.example.roomdatabase.DatePickerFragment;
import com.example.roomdatabase.R;
import com.example.roomdatabase.TimePickerFragment;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class AddMedicine extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, CancelListener {

    private static final String CHANNEL_ID = "1";
    Calendar c;
    EditText etMediName, etDuration;
    SharedPreferences sharedPreferences;
    private int duration;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medicine);

        Button buttonSetTime = findViewById(R.id.button_add_set_time);
        Button buttonSetDate = findViewById(R.id.button_add_set_date);
        Button buttonSetAlarm = findViewById(R.id.button_add_set_alarm);
        etMediName = findViewById(R.id.et_add_name);
        etDuration = findViewById(R.id.et_add_medicine_duration);
        sharedPreferences = this.getSharedPreferences(this.getString(R.string.spMedicines), Context.MODE_PRIVATE);
        c = Calendar.getInstance();
        c.set(Calendar.SECOND, 0);
        context = this;

        buttonSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment datePicker = new DatePickerFragment();
                datePicker.show(getSupportFragmentManager(), "date picker");
            }
        });

        buttonSetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePickerFragment();
                timePicker.show(getSupportFragmentManager(), "Time Picker");
            }
        });

        buttonSetAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etMediName.getText().toString();
                duration = -1;
                duration = Integer.parseInt(etDuration.getText().toString());
                if(duration<1)
                    duration = 1;
                startAlarm(c.getTimeInMillis(), name, duration);
                finish();
            }
        });
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        TextView textView = (TextView) findViewById(R.id.tv_add_time);
        String hourString = String.valueOf(hourOfDay);
        String apm = "AM";
        if (hourOfDay >= 12) {
            int hourInt = hourOfDay - 12;
            if (hourInt == 0)
                hourInt = 12;
            hourString = String.valueOf(hourInt);
            apm = "PM";
        }
        String finalTime = hourString + ":" + String.valueOf(minute) + " " + apm;
        textView.setText(finalTime);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar cc = Calendar.getInstance();
        cc.set(Calendar.YEAR, year);
        cc.set(Calendar.MONTH, month);
        cc.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String currentDateString = DateFormat.getDateInstance().format(cc.getTime());

        TextView textView = (TextView) findViewById(R.id.tv_add_date);
        textView.setText(currentDateString);
    }

    public void startAlarm(long timeInMillis, String name, int duration) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int reqCode = sharedPreferences.getInt(String.valueOf(R.string.lastRequestCode), 0);
        reqCode++;
        reqCode = reqCode % 100000000;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(name, reqCode).apply();

        for (int i = 1; i <= duration; i++) {
            reqCode++;
            reqCode = reqCode % 100000000;
            Intent intent = new Intent(getApplicationContext(), AlertReceiver.class);
            intent.putExtra("reqCode", reqCode);
            intent.putExtra("medicineName", name);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), reqCode, intent, 0);
            if (Build.VERSION.SDK_INT >= 19)
                alarmManager.setExact(AlarmManager.RTC, timeInMillis, pendingIntent);
            else
                alarmManager.set(AlarmManager.RTC, timeInMillis, pendingIntent);
            timeInMillis += TimeUnit.DAYS.toMillis(1);
            Toast.makeText(getApplicationContext(), "Alarm Set", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void cancelAlarm(int reqCode) {
        // AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // Intent intent = new Intent(getApplicationContext(), AlertReceiver.class);
        // PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), reqCode, intent, 0);
        // alarmManager.cancel(pendingIntent);
    }
}