package com.example.roomdatabase.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.roomdatabase.R;
import com.example.roomdatabase.UpdateListener;

public class AlarmUi extends AppCompatActivity {

    TextView tvMediName;
    // UpdateListener updateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_ui);

        String mediName = getIntent().getStringExtra("medicineName");
        tvMediName = findViewById(R.id.tv_alarm_ui_medicine_name);
        tvMediName.setText(mediName);
        // updateListener = (UpdateListener) getIntent().getSerializableExtra("listener");

        new CountDownTimer(60000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                finish();
            }
        }.start();
    }

    public void setIvCancel(View view){
        finish();
    }

}