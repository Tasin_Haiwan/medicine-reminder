package com.example.roomdatabase;

public interface CancelListener {
    void cancelAlarm(int reqCode);
}
