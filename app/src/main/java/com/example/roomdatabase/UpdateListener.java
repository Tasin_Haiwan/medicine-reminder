package com.example.roomdatabase;

public interface UpdateListener {
    void update();
}
