package com.example.roomdatabase;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.MedicineViewHolder>{

    private Context context;
    private List<MedicineInfo> list;
    private CancelListener cancelListener;
    private SharedPreferences sharedPref;
    private UpdateListener updateListener;

    public MedicineAdapter(Context context, List<MedicineInfo> list, SharedPreferences sharedPref) {
        this.context = context;
        this.list = list;
        this.sharedPref = sharedPref;
    }

    public void setList(List<MedicineInfo> list){
        this.list = list;
        notifyDataSetChanged();
    }

    public void setUpdateListener(UpdateListener updateListener) {
        this.updateListener = updateListener;
    }

    @NonNull
    @Override
    public MedicineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.single_layout, parent, false);
        return new MedicineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MedicineViewHolder holder, int position) {
        String name = list.get(position).getMedicineName();
        holder.pillName.setText(name);
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                Intent intent = new Intent(context, AlertReceiver.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context, list.get(position).getReqCode(), intent, 0);
                alarmManager.cancel(pendingIntent);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove(name).apply();
                updateListener.update();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class MedicineViewHolder extends RecyclerView.ViewHolder{
        TextView pillName, startDate, duration;
        Button delete;
        public MedicineViewHolder(@NonNull View itemView) {
            super(itemView);
            pillName = itemView.findViewById(R.id.tv_single_layout_pill_name);
            // startDate = itemView.findViewById(R.id.tv_single_layout_starting_date);
            // duration = itemView.findViewById(R.id.tv_single_layout_duration);
            delete = itemView.findViewById(R.id.button_single_layout_cancel);
        }
    }
}